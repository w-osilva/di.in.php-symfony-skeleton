<?xml version="1.0" encoding="UTF-8"?>

<project name="Symfony Skeleton" default="build">
    <property file="build.properties"/>
    <target name="build"
            depends="build-update"/>

    <target name="build-full" depends="install-dev, run-qa"/>
    <target name="build-ci" depends="install-test, run-qa"/>

    <target name="build-update" depends="update,run-qa"/>

    <target name="install-test" depends="create-config-test, install"/>
    <target name="install-dev" depends="dialog-continue, create-config-dev, install"/>
    <target name="install-prod" depends="dialog-continue, create-config-prod, install "/>

    <target name="install"
            depends="setup-appdirs, clean-builddirs, setup-builddirs, composer-install, composer-update, phpab, install-application"
            description="Prepare for build on install"/>

    <target name="update"
            depends="setup-appdirs, clean-builddirs, setup-builddirs, composer-update, phpab, update-application"
            description="Prepare for build on update">
    </target>

    <target name="run-qa" depends="lint, phploc, pdepend, phpmd-ci, phpcs-ci,phpcpd,  phpunit, phpdox, phpcb"/>

    <target name="dialog-continue">
        <input message="All data is going to be deleted and application is setup freshly. Do you want to continue (y/n)?"
                validargs="y,n"
                addproperty="do.continue"/>
        <condition property="do.abort">
            <equals arg1="n" arg2="${do.continue}"/>
        </condition>
        <fail if="do.abort">Install aborted by user.</fail>
    </target>

    <target name="clean-builddirs" description="Cleanup build artifacts">
        <delete dir="${basedir}/${project.buildDir}/api"/>
        <delete dir="${basedir}/${project.buildDir}/code-browser"/>
        <delete dir="${basedir}/${project.buildDir}/coverage"/>
        <delete dir="${basedir}/${project.buildDir}/logs"/>
        <delete dir="${basedir}/${project.buildDir}/pdepend"/>
        <delete dir="${basedir}/${project.buildDir}/phpdox"/>
    </target>

    <target name="setup-builddirs">
        <mkdir dir="${basedir}/${project.buildDir}/api"/>
        <mkdir dir="${basedir}/${project.buildDir}/code-browser"/>
        <mkdir dir="${basedir}/${project.buildDir}/coverage"/>
        <mkdir dir="${basedir}/${project.buildDir}/logs"/>
        <mkdir dir="${basedir}/${project.buildDir}/pdepend"/>
        <mkdir dir="${basedir}/${project.buildDir}/phpdox"/>
    </target>

    <target name="setup-appdirs" description="set permissions for the directorys">
        <exec executable="chmod">
            <arg value="-R"/>
            <arg value="777"/>
            <arg value="${basedir}/symfony/app/cache"/>
        </exec>
        <exec executable="chmod">
            <arg value="-R"/>
            <arg value="777"/>
            <arg value="${basedir}/symfony/app/logs"/>
        </exec>
    </target>
    <target name="create-config-prod" description="Install Production config">
        <copy file="symfony/app/config/parameters.yml.prod" tofile="symfony/app/config/parameters.yml" overwrite="true"/>
    </target>
    <target name="create-config-dev" description="Install Dev config">
        <copy file="symfony/app/config/parameters.yml.dev" tofile="symfony/app/config/parameters.yml" overwrite="true"/>
    </target>
    <target name="create-config-test" description="Install Test config">
        <copy file="symfony/app/config/parameters.yml.test" tofile="symfony/app/config/parameters.yml" overwrite="true"/>
    </target>
    <target name="composer-install" description="Composer update dependencies">
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="curl -s https://getcomposer.org/installer | php"/>
        </exec>
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php composer.phar install -d symfony/"/>
        </exec>
    </target>
    <target name="composer-update" description="Composer update dependencies">
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php composer.phar selfupdate"/>
        </exec>
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php composer.phar update -d symfony/"/>
        </exec>
    </target>

    <target name="install-application" description="sets up the application data and database">
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php symfony/app/console doctrine:database:drop --force"/>
        </exec>
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php symfony/app/console doctrine:database:create"/>
        </exec>
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php symfony/app/console doctrine:schema:update --force"/>
        </exec>
        <!--uncomment this, if you are using sonate user bundle
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php symfony/app/console sonata:admin:setup-acl"/>
        </exec>-->
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php symfony/app/console assets:install symfony/web/ --symlink --relative"/>
        </exec>
       <!-- uncomment if you are using dictrine fixtures
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="echo 'y'| php symfony/app/console doctrine:fixtures:load"/>
        </exec>-->
    </target>

    <target name="update-application" description="sets up the application data and database">
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php symfony/app/console doctrine:schema:update --force"/>
        </exec>
        <!-- uncomment this, if you are using sonate user bundle
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php symfony/app/console sonata:admin:setup-acl"/>
        </exec>-->
        <exec executable="bash">
            <arg value="-c"/>
            <arg value="php symfony/app/console assets:install symfony/web/ --symlink --relative"/>
        </exec>
    </target>

    <target name="phpab" description="Generate autoloader scripts">
        <exec executable="${basedir}/${project.bin}/phpab">
            <arg value="--output"/>
            <arg path="${basedir}/${project.srcDir}/autoload.php"/>
            <arg path="${basedir}/${project.srcDir}"/>
        </exec>
    </target>


    <target name="lint">
        <apply executable="php" failonerror="true">
            <arg value="-l"/>
            <fileset dir="${basedir}/${project.srcDir}">
                <include name="**/*.php"/>
                <modified/>
            </fileset>
        </apply>
    </target>

    <target name="phploc" description="Measure project size using PHPLOC">
        <exec executable="${basedir}/${project.bin}/phploc">
            <arg value="--log-csv"/>
            <arg value="${basedir}/${project.buildDir}/logs/phploc.csv"/>
            <arg value="--log-xml"/>
            <arg value="${basedir}/${project.buildDir}/logs/phploc.xml"/>
            <arg path="${basedir}/${project.srcDir}"/>
        </exec>
    </target>

    <target name="pdepend"
            description="Calculate software metrics using PHP_Depend">
        <exec executable="${basedir}/${project.bin}/pdepend">
            <arg value="--jdepend-xml=${basedir}/${project.buildDir}/logs/jdepend.xml"/>
            <arg value="--jdepend-chart=${basedir}/${project.buildDir}/pdepend/dependencies.svg"/>
            <arg value="--overview-pyramid=${basedir}/${project.buildDir}/pdepend/overview-pyramid.svg"/>
            <arg path="${basedir}/${project.srcDir}"/>
        </exec>
    </target>

    <target name="phpmd"
            description="Perform project mess detection using PHPMD">
        <exec executable="${basedir}/${project.bin}/phpmd">
            <arg path="${basedir}/${project.srcDir}"/>
            <arg value="text"/>
            <arg value="${basedir}/${project.buildDir}/phpmd.xml"/>
        </exec>
    </target>

    <target name="phpmd-ci"
            description="Perform project mess detection using PHPMD">
        <exec executable="${basedir}/${project.bin}/phpmd">
            <arg path="${basedir}/${project.srcDir}"/>
            <arg value="xml"/>
            <arg value="${basedir}/${project.buildDir}/phpmd.xml"/>
            <arg value="--reportfile"/>
            <arg value="${basedir}/${project.buildDir}/logs/pmd.xml"/>
            <arg value="--exclude"/>
            <arg value="models/*,admin/*"/>
        </exec>
    </target>

    <target name="phpcs"
            description="Find coding standard violations using PHP_CodeSniffer">
        <exec executable="${basedir}/${project.bin}/phpcs">
            <arg value="--standard=PSR2"/>
            <arg value="--ignore=autoload.php,*.js,*.css"/>
            <arg path="${basedir}/${project.srcDir}"/>
        </exec>
    </target>

    <target name="phpcs-ci"
            description="Find coding standard violations using PHP_CodeSniffer">
        <exec executable="${basedir}/${project.bin}/phpcs" output="/dev/null">
            <arg value="--report=checkstyle"/>
            <arg value="--report-file=${basedir}/${project.buildDir}/logs/checkstyle.xml"/>
            <arg value="--standard=PSR2"/>
            <arg value="--ignore=autoload.php,*.js,*.css"/>
            <arg path="${basedir}/${project.srcDir}"/>
        </exec>
    </target>

    <target name="phpcpd" description="Find duplicate code using PHPCPD">
        <exec executable="${basedir}/${project.bin}/phpcpd">
            <arg value="--log-pmd"/>
            <arg value="${basedir}/${project.buildDir}/logs/pmd-cpd.xml"/>
            <arg path="${basedir}/${project.srcDir}"/>
        </exec>
    </target>

    <target name="phpdox"
            description="Generate API documentation using phpDox">
        <exec executable="${basedir}/${project.bin}/phpdox">
        </exec>
    </target>

    <target name="phpunit" description="Run unit tests with PHPUnit">
        <exec executable="${basedir}/${project.bin}/phpunit" failonerror="true">
            <arg value="-d"/>
            <arg value="zend.enable_gc=0"/>
        </exec>
    </target>

    <target name="phpcb"
            description="Aggregate tool output with PHP_CodeBrowser">
        <exec executable="${basedir}/${project.bin}/phpcb">
            <arg value="--log"/>
            <arg path="${basedir}/${project.buildDir}/logs"/>
            <arg value="--source"/>
            <arg path="${basedir}/${project.srcDir}"/>
            <arg value="--output"/>
            <arg path="${basedir}/${project.buildDir}/code-browser"/>
        </exec>
    </target>
</project>
